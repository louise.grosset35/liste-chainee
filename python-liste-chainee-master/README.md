# Python - Liste chainée
## Contexte
On désire modéliser une liste ordonnée et une file d’entiers en créant deux classes `OrderedList` et `File` implémentant toutes les deux la classe abstraite `ChainList` définie comme suit : 
```
abstract class ChainList { 
	insert(Element e) ; 
	delete(Element e) ; 
	boolean is_empty() ; 
}
```

Les éléments de la pile et de la file sont de type Element  défini comme suit : 
```
class Element { 
	int value ; 
	Element next ; 
} 
```
La liste chainée ne stocke que la tête de la liste et chaque élément connait son succésseur.
L'attribut `next` de l'Element référençant l'élément suivant dans la liste chaînée.

```mermaid
classDiagram
direction LR
    A --> B: next
    B --> C: next
    C --> E: next
    class A{
      +int value
      +Element next
    }
    class B{
      +int value
      +Element next
    }
    class C{
      +int value
      +Element next
    }
    class E{
      +int value
      +Element next
    }
```

## Consignes
L'ensemble du travail doit être réalisé au sein d'un fichier `main.py`.

```mermaid
classDiagram
    Element --* ChainList
    ChainList <|-- OrderedList
    ChainList <|-- File
    
    class Element{
      +int value
      +Element next
      +__eq__() bool
      +__str__() str
    }
    class ChainList{
      +Element top
      +insert(Element e)
      +delete(Element e)
      +is_empty() bool
      +__len__() int 
      +__str__() str
    }
    class OrderedList{
      +insert(Element e)
      +delete(Element e)
    }
    class File{
      +insert(Element e)
      +delete(Element e)
    }
```

1. Ecrire une classe `File` implémentant l'interface `ChainList`. Implémenter les méthodes nécessaires.
   - La File doit fonctionner en mode FIFO, c'est à dire First In First Out. 
   - Le premier élément rajouté est en tête de File, le dernier élément ajouté est à la fin de la liste chainée.

2. Ecrire une classe `OrderedList` implémentant l'interface `ChainList`. Implémenter les méthodes nécessaires. 
	- Une OrderedList est automatiquement triée du plus petit au plus grand. 
    - Le plus petit élément est donc au sommet de la `OrderedList`

NB : La gestion des exceptions pouvant être déclenchées par les opérations d’entrées sorties doit être prise en compte. 


## Consignes bonus

1. Faites en sorte pour que les méthodes insert et delete puissent accepter plusieurs paramètres : `f.insert(Element(0), Element(12), Element(15))`
2. Implémenter un nouveau type de Liste chainée nommée `Pile`de type LIFO (Lest in First Out)
3. Implémenter un nouveau type de Liste chainée, le `Set`, qui ne conserve qu'une seul occurence d'Element
4. Implémenter la méthode `get(n)`, permettant de récupérer le nième enregistrement.
5. Adaptez votre liste chainée pour qu'elle soit doublement chainée. C'est à dire qu'un Element ait deux attributs : `next` et `prev`.