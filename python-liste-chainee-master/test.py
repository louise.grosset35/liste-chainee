from main import File, OrderedList, Element
import re

def test_files():
    # Files are FIFO lists
    f = File()
    assert(f.is_empty()) # File is empty initially
    f.insert(Element(15))
    assert(not f.is_empty()) # File is no longer empty
    f.insert(Element(20))
    f.insert(Element(13))
    assert bool(re.search(str(f), '15.?20.?13')) # String representation should contain values 1 and 20 and 13
    assert len(f) == 3  # assert that the method __len__ is properly set
    assert f.top == Element(15) ## assert that first element is first element imported

    try:
        f.delete(Element(42))
    except:
        pass # Exception was properly raised
    else:
        assert False # The removal of a non existing item should raise an exception.
def test_ordered_lists():


    # Piles are ordered lists
    p = OrderedList()
    assert (p.is_empty())  # Pile is empty initially
    p.insert(Element(1))
    assert (not p.is_empty())  # Pile is no longer empty
    p.insert(Element(20))
    p.insert(Element(13))
    assert bool(re.search(str(p), '1.?13.?20'))  # String representation should contain ordered values 1 and 20 and 13
    p.delete(Element(13))
    p.insert(Element(0))
    assert bool(re.search(str(p), '0.?1.?20'))  # String representation should contain ordered values 1 and 20 and 0
    assert len(p) == 3 # assert that the method __len__ is properly set
    assert p.top == Element(0) ## assert that first element is the smallest element imported

    try:
        p.delete(Element(42))
    except:
        pass # Exception was properly raised
    else:
        assert False # The removal of a non existing item should raise an exception.

if __name__ == '__main__':
    test_files()
    test_ordered_lists()