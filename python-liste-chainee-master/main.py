from __future__ import annotations
from abc import ABC, abstractmethod


class Element:
	value: int
	next: Element

	def __init__(self, value:int, next: Element = None):
		"""
		Constructeur de mon Element
			:param value : la valeur stockée dans un Element d'une liste chaînée
			:param next : l'Element suivant dans la liste chaînée
		"""
		self.value = value
		self.next = next

	def __eq__(self, other: Element) -> bool:
		return self.value == other.value

	def __str__(self) -> str:
		return f"{self.value}"

	def element_next(self):
		pass


class ChainList(ABC):
	top: Element

	def __init__(self):
		self.top = None

	@abstractmethod
	def insert(self, e: Element):
		pass

	def delete(self, e: Element):
		if self.top == e:
			self.top = self.top.next
		else:
			previous = self.top
			current = previous.next
			while current is not None and not current.__eq__(previous):
				previous, current = current, previous.next
			if current is None:
				raise Exception('Existe pas')
			else:
				previous.next = current.next

	def is_empty(self):
		return self.top is None

	def __str__(self):
		r =''
		e = self.top
		while e is not None:
			r = r + str(e) + '->'
			e = e.next
		return r

	def __len__(self):
		n = 0
		a = self.top
		while a is not None:
			n = n+1
			a = a.next
		return n


class File(ChainList):
	def insert(self, e: Element):
		if self.is_empty():
			self.top = e
		else:
			current = self.top
			while current.next is not None:
				current = current.next
			current.next = e


class OrderedList(ChainList):
	def insert(self, e: Element):
		if self.is_empty():
			self.top = e
		else:
			current = self.top
			while current.value < e.value:
				current = current.next
			a = current.next
			current.next = e.next
			e.next = a


class Pile(ChainList):
	def insert(self, e: Element):
		if self.is_empty():
			self.top = e
		else:
			e, e.next = self.top, self.top.next


class Set(ChainList):
	def insert(self, e: Element):
		if self.is_empty():
			self.top = e
		else:
			previous = self.top
			current = previous.next
			while current is not None and not current.__eq__(previous):
				previous, current = current, previous.next
			if current is None:
				current.next = e
			else:
				print("Cette valeur existe déjà")
